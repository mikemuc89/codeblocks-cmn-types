/* @flow */
import MESSAGES from './messages.json';

const formatMessage = (message: string, ...args: Array<string | number>) =>
  args.reduce(
    (resultMessage: string, currentArg: string | number) => resultMessage.replace(/{}/, `${currentArg}`),
    message
  );

export const ERR_FLD_VALUE_REQUIRED = () => MESSAGES.ERR_FLD_VALUE_REQUIRED.pl;

export const ERR_FLD_INVD_VALUE = () => MESSAGES.ERR_FLD_INVD_VALUE.pl;

export const ERR_FLD_VALUE_TOO_LONG = (maxLen?: number, actualLen?: number) =>
  (maxLen && actualLen && formatMessage(MESSAGES.ERR_FLD_VALUE_TOO_LONG_MAX_ACTUAL.pl, maxLen, actualLen)) ||
  (maxLen && formatMessage(MESSAGES.ERR_FLD_VALUE_TOO_LONG_MAX.pl, maxLen)) ||
  MESSAGES.ERR_FLD_VALUE_TOO_LONG.pl;

export const ERR_FLD_VALUE_TOO_SHORT = (minLen?: number, actualLen?: number) =>
  (minLen && actualLen && formatMessage(MESSAGES.ERR_FLD_VALUE_TOO_SHORT_MIN_ACTUAL.pl, minLen, actualLen)) ||
  (minLen && formatMessage(MESSAGES.ERR_FLD_VALUE_TOO_SHORT_MIN.pl, minLen)) ||
  MESSAGES.ERR_FLD_VALUE_TOO_SHORT.pl;

export const ERR_FLD_VALUE_TOO_BIG = (maxValue?: number, actualValue?: number) =>
  (maxValue && actualValue && formatMessage(MESSAGES.ERR_FLD_VALUE_TOO_BIG_MAX_ACTUAL.pl, maxValue, actualValue)) ||
  (maxValue && formatMessage(MESSAGES.ERR_FLD_VALUE_TOO_BIG_MAX.pl, maxValue)) ||
  MESSAGES.ERR_FLD_VALUE_TOO_BIG.pl;

export const ERR_FLD_VALUE_TOO_SMALL = (minValue?: number, actualValue?: number) =>
  (minValue && actualValue && formatMessage(MESSAGES.ERR_FLD_VALUE_TOO_SMALL_MIN_ACTUAL.pl, minValue, actualValue)) ||
  (minValue && formatMessage(MESSAGES.ERR_FLD_VALUE_TOO_SMALL_MIN.pl, minValue)) ||
  MESSAGES.ERR_FLD_VALUE_TOO_SMALL.pl;

export const ERR_FLD_INVD_ITEM_SELECTED = () => MESSAGES.ERR_FLD_INVD_ITEM_SELECTED.pl;

export const ERR_FLD_INVD_RANGE = (bottom?: number, top?: number) =>
  (bottom !== undefined &&
    top !== undefined &&
    formatMessage(MESSAGES.ERR_FLD_INVD_RANGE_BOTTOM_TOP.pl, bottom, top)) ||
  MESSAGES.ERR_FLD_INVD_RANGE.pl;

export const ERR_FLD_INVD_VALUE_REGEX = () => MESSAGES.ERR_FLD_INVD_VALUE_REGEX.pl;

export const ERR_FLD_INVD_USER_PASS = () => MESSAGES.ERR_FLD_INVD_USER_PASS.pl;

export const ERR_FLD_NOT_MATCHING_PASS = () => MESSAGES.ERR_FLD_NOT_MATCHING_PASS.pl;

export const ERR_FLD_NOT_MATCHING_EMAIL = () => MESSAGES.ERR_FLD_NOT_MATCHING_EMAIL.pl;

export const ERR_FLD_MAX_DATE_EXCEEDED = (maxDate?: string, actualDate?: string) =>
  (maxDate && actualDate && formatMessage(MESSAGES.ERR_FLD_MAX_DATE_EXCEEDED_MAX_ACTUAL.pl, maxDate, actualDate)) ||
  (maxDate && formatMessage(MESSAGES.ERR_FLD_MAX_DATE_EXCEEDED_MAX.pl, maxDate)) ||
  MESSAGES.ERR_FLD_MAX_DATE_EXCEEDED.pl;

export const ERR_FLD_MIN_DATE_EXCEEDED = (minDate?: string, actualDate?: string) =>
  (minDate && actualDate && formatMessage(MESSAGES.ERR_FLD_MIN_DATE_EXCEEDED_MIN_ACTUAL.pl, minDate, actualDate)) ||
  (minDate && formatMessage(MESSAGES.ERR_FLD_MIN_DATE_EXCEEDED_MIN.pl, minDate)) ||
  MESSAGES.ERR_FLD_MIN_DATE_EXCEEDED.pl;

export const ERR_FLD_INVD_CHARACTERS = () => MESSAGES.ERR_FLD_INVD_CHARACTERS.pl;

export const ERR_FRM_INVD_VALUES = () => MESSAGES.ERR_FRM_INVD_VALUES.pl;

export const ERR_REQ_INVD_REQUEST = () => MESSAGES.ERR_REQ_INVD_REQUEST.pl;
