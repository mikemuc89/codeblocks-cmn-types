/* @flow */
import * as React from 'react';

// Components
export type HocWrappedComponentType = React.ComponentType<any>;

// Callbacks
export type GenericCallbackType<ArgType, ReturnType> = (arg: ArgType) => ReturnType;
export type EmptyCallbackType = () => void;
export type FocusCallbackType = GenericCallbackType<FocusEvent, void>;
export type StateSetterType<StateType> = (oldState: StateType) => StateType;
export type SetStateCallbackType<StateType> = (stateSetter: StateSetterType) => void | ((value: StateType) => void);
export type SimpleEventCallbackType = GenericCallbackType<Event, void>;
export type ValueCallbackType<ValueType> = GenericCallbackType<ValueType, void>;

// Refs
export type ReactRefType<ElementType> = { current: null | ElementType };
export type ReactNotNullableRefType<ElementType> = { current: ElementType };
export type ReactForwardedRefType = ReactRefType<HTMLElement>;

// Errors
export type ErrorObjectType = {|
  description?: string,
  exception?: string,
  stack?: string,
  title: string,
  traceback?: string
|};
export type ErrorsType = null | string | ErrorObjectType;
export type WithMessagesType = {|
  errors?: ErrorsType,
  helper?: ErrorsType,
  hints?: ErrorsType,
  required?: boolean,
  warnings?: ErrorsType
|};

// Handlers
export type FieldHandlerArgType = {|
  field: Object,
  key: string,
  options: Object,
  updateFields: (Object, () => void) => void
|};

// Other
export type LocationType = {|
  host?: string,
  hostname?: string,
  href?: string,
  origin?: string,
  pathname: string,
  port?: string,
  protocol?: string,
  reload?: Function,
  replace?: Function,
  search?: string
|};

// Navigation
export type NavigationRefType = {|
  data: Object
|};

// React
export type ReactChildren = React.Node;
export type ReactElementsChildren<ChildType> = React.Element<ChildType> | Array<React.Element<ChildType>>;
